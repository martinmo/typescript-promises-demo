import {Response} from "./mocks";

namespace AsyncPromises {

    const wait = (millis: number) => new Promise((resolve) => setTimeout(resolve, millis));

    async function loadFromDb(connection: any, id: number): Promise<string> {
        console.log('start loading', id);
        await wait(10);
        console.log('loaded from db', id);
        if (id) {
            return Promise.resolve("Item " + id)
        } else {
            return Promise.reject(Error("invalid id"))
        }
    }

    async function getConnection(): Promise<string> {
        return Promise.resolve("connection")
    }

    function toDto(item: string): string {
        return item.toUpperCase()
    }

    // parallel
    async function requestHandler(req: any, resp: Response) {

        const ids = [1, 2, 3, 4];

        try {
            const asyncDtos = ids.map(async id => {
                const connection = await getConnection();
                const item = await loadFromDb(connection, id);
                return toDto(item);
            });

            const dtos = await Promise.all(asyncDtos);

            resp.send(dtos);
        } catch (err) {
            resp.sendError(err)
        }
    }

    // in order
    async function requestHandler2(req: any, resp: Response) {

        const ids = [1, 2, 3, 4];

        try {

            const result = [];

            for(const id of ids){
                const connection = await getConnection();
                const item = await loadFromDb(connection, id);
                result.push(toDto(item));
            }

            resp.send(result);
        } catch (err) {
            resp.sendError(err)
        }
    }


    requestHandler({}, new Response());
    //requestHandler2({}, new Response());
}