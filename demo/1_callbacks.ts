import {Response} from "./mocks";

namespace callbacks {

    function loadFromDb(connection: any, id: number, cb: (err?: Error, item?: string) => void) {
        if (id) {
            cb(undefined, "Item " + id)
        } else {
            cb(Error("invalid id"), undefined)
        }
    }

    function getConnection(cb: (err?: Error, connection?: string) => void) {
        cb(undefined, "connection")
    }

    function toDto(item?: string): string {
        if(item) {
            return item.toUpperCase()
        }else{
            throw new Error("to dto failed")
        }
    }

    function requestHandler(req: any, resp: Response) {
        getConnection((err, connection) => {
            if (err) {
                resp.sendError(err);
            } else {
                loadFromDb(connection, req.id, (err, item) => {
                    if (err) {
                        resp.sendError(err);
                    } else {
                        const dto = toDto(item);
                        resp.send(dto)
                    }
                })
            }
        })
    }


    requestHandler({id: 1}, new Response());
    requestHandler({}, new Response());
}