
export class Response {
    public send(body: any) {
        console.log("HTTP 200 OK, with body:", body)
    }

    public sendError(body: any) {
        console.log("HTTP 500 Server Error, with body:", body)
    }
}