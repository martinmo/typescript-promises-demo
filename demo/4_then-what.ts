// https://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html

const wait = (millis: number) => new Promise((resolve) => setTimeout(resolve, millis));


function longFunction(): Promise<any> {
    console.log("longFunction");
    return wait(20).then(() => {
        console.log("longFunction done");
        return "longFunctionResult"
    });
}

function shortFunction(): Promise<any> {
    console.log("shortFunction");
    return wait(10).then(() => {
        console.log("shortFunction done");
        return "shortFunctionResult"
    });
}

console.log("-- begin");

/*
    what will be printed?
 */

longFunction().then(() => {
    return shortFunction();
})

/*
    what will be printed?
 */

// longFunction().then(() =>
//     shortFunction()
// )

/*
    what will be printed?
 */

// longFunction().then(shortFunction())

/*
    what will be printed?
 */

// longFunction()
//     .then(shortFunction)
//     .then(shortFunction)


    .then(some => console.log("-- done. Last result:", some));


//@formatter:off
/*
    Scroll down for explanation


                ||
                ||
               \  /
                \/







































 then callback returns

 - value -> resolve(value)
 - Promise -> Promise
 - void -> resolve(undefined)


 then callback not a function -> resolve(?)

 */

