import {Response} from "./mocks";

namespace Cb2Promises {

    /*
     new Promise((resolve:Function, reject:Function) => void)

     new Promise(f: (resolve: (result: Promise | any) => void, reject: (err: any) => void) => void)
     */

    new Promise((resolve, reject) => {

        if (Date.now() % 2 === 0) {
            resolve('created a promise and it is good')
        } else {
            reject('failed')
        }

    })
        .then(console.log)
        .catch(console.error);


    /*
        already done promise
    */
    Promise.resolve('always good')
        .then(console.log);

    Promise.reject('never good')
        .catch(console.error);


    /*
     expection while creating promise
     */
    const failed = new Promise((resolve, reject) => {

        throw Error('failed');

    })
        .then(() => console.log('will not happen'))
        .catch(err => console.error('catched error that occured while building promise', err));


    /*
        CB based method
     */
    function loadFromDb(connection: any, id: number, cb: (err?: Error, item?: string) => void) {
        if (id) {
            cb(undefined, "Item " + id)
        } else {
            cb(Error("invalid id"), undefined)
        }
    }

    /*
        transformed to return a promise
    */
    function loadFromDbAsync(connection: any, id: number): Promise<string> {
        return new Promise((resolve, reject) => {
            loadFromDb(connection, id, (err, item) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(item)
                }
            })
        });
    }

    function getConnection(): Promise<string> {
        return Promise.resolve("connection")
    }

    function toDto(item: string): string {
        return item.toUpperCase()
    }

    function requestHandler(req: any, resp: Response) {
        getConnection()
            .then((connection) => loadFromDbAsync(connection, req.id))
            .then(toDto)
            .then(dto => resp.send(dto))
            .catch(err => resp.sendError(err));
    }


    requestHandler({id: 1}, new Response());
    requestHandler({}, new Response());
}