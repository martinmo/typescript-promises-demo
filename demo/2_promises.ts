import {Response} from "./mocks";

namespace Promises {

    function loadFromDb(connection: any, id: number): Promise<string> {
        if (id) {
            return Promise.resolve("Item " + id)
        } else {
            return Promise.reject(Error("invalid id"))
        }
    }

    function getConnection(): Promise<string> {
        return Promise.resolve("connection")
    }

    function toDto(item: string): string {
        return item.toUpperCase()
    }

    function requestHandler(req: any, resp: Response) {
        getConnection()
            .then((connection) => loadFromDb(connection, req.id))
            .then(item => toDto(item))
            .then(dto => resp.send(dto))
            .catch(err => resp.sendError(err + ' last'));
    }


    requestHandler({id: 1}, new Response());
    requestHandler({}, new Response());
}