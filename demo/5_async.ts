import {Response} from "./mocks";

namespace AsyncPromises {

    async function loadFromDb(connection: string, id: number): Promise<string> {
        if (id) {
            return Promise.resolve("Item " + id)
        } else {
            return Promise.reject(Error("invalid id"))
        }
    }

    async function getConnection(): Promise<string> {
        return Promise.resolve("connection")
    }

    function toDto(item: string): string {
        return item.toUpperCase()
    }

    async function requestHandler(req: any, resp: Response) {
        try {
            const connection = await getConnection();
            const item = await loadFromDb(connection, req.id);
            const dto = toDto(item);
            resp.send(dto);
        } catch (err) {
            resp.sendError(err)
        }
    }


    requestHandler({id: 1}, new Response());
    requestHandler({}, new Response());
}