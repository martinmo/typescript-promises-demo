import * as express from "express";
import {Express, NextFunction, Request, Response} from "express";
import errorHandler = require("errorhandler");

namespace ExpressPromises {

    async function loadFromDb(connection: any, id: number): Promise<string> {
        if (Number.isInteger(id)) {
            return Promise.resolve("Item " + id)
        } else {
            return Promise.reject(Error("invalid id"))
        }
    }

    async function getConnection(): Promise<string> {
        return Promise.resolve("connection")
    }

    const app: Express = express();

    app.get('/:some', (req: Request, res: Response, next: NextFunction) => {
        if (req.params['some'] === 'fail') {
            next('failed')
        } else {
            res.send('hi')
        }
    });

    app.get('/cb/:id', (req: Request, res: Response, next: NextFunction) => {

        getConnection()
            .then(con => {
                return loadFromDb(con, parseFloat(req.params['id']));
            })
            .then(item => {
                res.send(item);
            })
            .catch(err => {
                next(err);
            });
    });

    app.get('/async/:id', async (req: Request, res: Response, next: NextFunction) => {

        try {
            const connection = await getConnection();
            const item = await loadFromDb(connection, parseFloat(req.params['id']));
            res.send(item);
        } catch (err) {
            next(err)
        }
    });

    function wrap(handler: (req: Request, res: Response) => Promise<any>) {
        return (req: Request, res: Response, next: NextFunction) => {
            handler(req, res).catch(err => next(err))
        }
    }

    app.get('/async2/:id', wrap(async (req: Request, res: Response) => {

        const connection = await getConnection();
        const item = await loadFromDb(connection, parseFloat(req.params['id']));
        res.send(item);

    }));

    app.use(errorHandler());
    app.listen(3000, () => console.log('listing on port 3000'))
}