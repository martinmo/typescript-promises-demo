import * as assert from 'assert';
import {error} from 'util';


async function loadFromDb(connection: any, id: number): Promise<string> {
    if (Number.isInteger(id)) {
        return Promise.resolve("Item " + id)
    } else {
        return Promise.reject(Error("invalid id"))
    }
}

async function getConnection(): Promise<string> {
    return Promise.resolve("connection")
}


describe('call backs', () => {

    it('good case', (done) => {
        getConnection()
            .then(con => loadFromDb(con, 1))
            .then(item => {
                //assert.equal(true, false);
                assert.ok(item);
                done()
            })
            .catch(err => done(err))
    });

    it('bad case', (done) => {
        getConnection()
            .then(con => loadFromDb(con, 1.1))
            .then(item => {
                done("should not happen")
            })
            .catch(err => {
                assert.equal(err.message, 'invalid id');
                //assert.equal(err.message, 'bla');
                done();
            })
    });
});

describe('promises', () => {

    //it('good case', (done) => {
    it('good case', () =>
        getConnection()
            .then(con => loadFromDb(con, 1))
            .then(item => {
                //assert.equal(true, false);
                assert.ok(item);
            })
    );

    it('bad case', (done) => {
        getConnection()
            .then(con => loadFromDb(con, 1.1))
            .then(item => {
                done("should not happen")
            })
            .catch(err => {
                assert.equal(err.message, 'invalid id');
                //assert.equal(err.message, 'bla');
                done();
            })
    });
});

describe('async', () => {

    // it('good case', (done) => {
    it('good case', async () => {
        const connection = await getConnection();
        const item = await loadFromDb(connection, 1);
        assert.ok(item);
    });

    it('bad case', async () => {
        const connection = await getConnection();
        try {
            const item = await loadFromDb(connection, 1.1);
            error('no exception')
        } catch (err) {
            assert.equal(err.message, 'invalid id');
        }

    });
});

